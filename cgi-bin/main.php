<?php

function init($dir)
{
    if (file_exists($dir)) {
        echo "OK";
    } else {
        mkdir($dir, 0777);
        echo "OK";
    }
}

function listFiles($dir)
{
    echo json_encode(scandir($dir));
}

function get($id, $type, $dir)
{
    $file = fopen($dir . '/' . $id . '.' . $type, "r");
    if(filesize($dir . '/' . $id . '.' . $type) > 0)
    	echo fread($file,filesize($dir . '/' . $id . '.' . $type));
    else
    	echo "";
    fclose($file);
    save($id, $type, $dir, "");
    unlink($dir . '/' . $id . '.' . $type);
}
function get2($id, $type, $dir)
{
    $file = fopen($dir . '/' . $id . '.' . $type, "r");
    if(filesize($dir . '/' . $id . '.' . $type) > 0)
    	echo fread($file,filesize($dir . '/' . $id . '.' . $type));
    else
    	echo "";
    fclose($file);
}
function save($id, $type, $dir, $value)
{
    $file = fopen($dir . "/" . $id . '.' . $type, 'w');
    fwrite($file, $value);
    fclose($file);
}

$base = 'init';
$token = '7d4580a3910c54d62b46f24c397c8d59';
if(isset($_GET['t'])){
	if($_GET['t'] != $token){
		die();
	}

	if(isset($_GET['f'])){
		$f = $_GET['f'];
		if($f == 's'){
		    $content = trim(file_get_contents("php://input"));
			save($_GET['id'],$_GET['type'],$base,$content);
			echo 'OK';
		} else if($f == 'g') {
			get($_GET['id'],$_GET['type'],$base);
		} else if($f == 'g2') {
			get2($_GET['id'],$_GET['type'],$base);
		} else if($f == 'l') {
			listFiles($base);
		} else if($f == 'i'){
			init($base);
		}
		
	}
	
}

?>